# CernVM-FS CSI deployment

This driver provides read-only mounting of CVMFS volumes in CSI-enabled container orchestrators.
Requires Kubernetes 1.11+ and < 1.17

## Deploying to a new environment/cluster

### Prerequisites

Some preparation steps need to be performed with cluster-admin permissions.
This needs to be done only once per OpenShift cluster/environment.

Start a Docker container for installation (mounting this git repo in /project):
```
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-cvmfs gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:
```
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
export HELM_VERSION=2.13.1 # make sure to use the same Helm version found in .gitlab-ci.yml
curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/{helm,tiller} /usr/bin/
# create namespace and serviceaccounts for deployment
helm template /project/prerequisites/ | oc create -f -
# obtain the token for the Tiller service account
oc serviceaccounts get-token -n paas-infra-cvmfs tiller
```

The rest of the deployment will be handled by GitLab CI.
We need to create 2 [GitLab CI variables](https://docs.gitlab.com/ee/ci/variables/#via-the-ui) in this project
for each OpenShift environment (see environment definitions in [.gitlab-ci.yml](https://gitlab.cern.ch/paas-tools/infrastructure/cvmfs-csi-deployment/blob/master/.gitlab-ci.yml) for the exact
variable names for each env):

1. A variable `TILLER_TOKEN_<ENV>` with the token for the `tiller` service account obtained above

### Uninstall the CernVM-FS components

Start a Docker container for installation (mounting this git repo in /project):
```
docker run --rm -it -v $(pwd):/project -e TILLER_NAMESPACE=paas-infra-cvmfs gitlab-registry.cern.ch/paas-tools/openshift-client:v3.11.0 bash
```

Then in that container:
```
# login as cluster admin to the destination cluster
oc login https://openshift-test.cern.ch #or openshift-dev.cern.ch or openshift.cern.ch
# install tiller
export HELM_VERSION=2.13.1
curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx
mv --force linux-amd64/{helm,tiller} /usr/bin/
# initialize local tiller
helm init --client-only
helm plugin install https://github.com/adamreese/helm-local
command -v which || yum install -y which # helm local plugin needs `which`, install if missing
helm local start
helm local status
export HELM_HOST=":44134"
# delete cvmfs deployment, then prerequisites
helm del --purge cvmfs
helm template /project/prerequisites | oc delete -f -
```

### Testing changes in a component's Docker image

In order to test any component of the helm charts described in this repository, follow the instructions
described in the paragraph `Using work-in-progress Docker images for dev deployment` in
[here](https://openshiftdocs.web.cern.ch/openshiftdocs/Misc/helmDevelopmentNotes/)